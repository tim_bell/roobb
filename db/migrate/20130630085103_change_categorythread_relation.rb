class ChangeCategorythreadRelation < ActiveRecord::Migration
  def change
    remove_column :post_threads, :forum_id
    add_column :post_threads, :post_thread_id, :integer
  end
end
