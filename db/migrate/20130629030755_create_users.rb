class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.date :registered
      t.string :password

      t.timestamps
    end
  end
end
