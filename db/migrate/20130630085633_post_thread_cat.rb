class PostThreadCat < ActiveRecord::Migration
  def change
    remove_column :post_threads, :post_thread_id
    add_column :post_threads, :category_id, :integer

    remove_column :posts, :thread_id
    add_column :posts, :post_thread_id, :integer
  end
end
