class UpdateCache < ActiveRecord::Migration
  def up
    users = User.all
    users.each do |user|
      User.reset_counters(user.id, :posts)
    end

    threads = PostThread.all
    threads.each do |thread|
      PostThread.reset_counters(thread.id, :posts)
    end
  end

  def down
  end
end
