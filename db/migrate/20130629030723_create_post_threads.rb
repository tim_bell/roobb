class CreatePostThreads < ActiveRecord::Migration
  def change
    create_table :post_threads do |t|
      t.string :subject
      t.integer :forum_id
      t.date :last_post

      t.timestamps
    end
  end
end
