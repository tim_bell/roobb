class Postcache < ActiveRecord::Migration
  def up
    add_column :posts, :parsed_body, :text
  end

  def down
    remove_column :posts, :parsed_body
  end
end
