class UpdateUsersTable < ActiveRecord::Migration
  def change
      add_column :users, :role, :integer
      remove_column :users, :password
  end
end
