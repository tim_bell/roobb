class Removepostthreadid < ActiveRecord::Migration
  def change
    unless column_exists? :posts, :topic_id
      rename_column :posts, :post_thread_id, :topic_id
    end
    unless column_exists? :categories, :topic_count
      rename_column :categories, :thread_count, :topic_count
    end
  end
end
