class AddingUserToThread < ActiveRecord::Migration
  def up
    add_column :post_threads, :user_id, :integer
  end

  def down
    remove_column :post_threads, :user_id
  end
end
