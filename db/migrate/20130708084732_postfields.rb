class Postfields < ActiveRecord::Migration
  def up
    add_column :posts, :deleted, :boolean, :default =>false
    add_column :posts, :deleted_message, :text

    add_column :topics, :deleted, :boolean, :default =>false
    add_column :topics, :closed, :boolean, :default =>false
    add_column :topics, :deleted_message, :text
  end

  def down
    remove_column :posts, :deleted
    remove_column :posts, :deleted_message

    remove_column :topics, :deleted
    remove_column :topics, :closed
    remove_column :topics, :deleted_message
  end
end
