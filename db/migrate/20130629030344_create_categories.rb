class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.integer :post_count
      t.integer :thread_count

      t.timestamps
    end
  end
end
