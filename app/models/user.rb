class User < ActiveRecord::Base

  ROLE_ADMIN = 1
  ROLE_USER = 2

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  attr_accessible :registered, :username

  before_create :set_defaults

  validates :username, presence: true

  has_many :posts

  def to_param
    "#{id}-#{self.username.parameterize}"
  end

  def set_defaults
    self.role = ROLE_USER
  end

end
