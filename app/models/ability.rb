class Ability
  include CanCan::Ability

  def initialize(user)

    if !user
      can :read, Post
      can :read, Topic
      can :read, Category
      can :read, User
    elsif user.role == User::ROLE_ADMIN
      can :manage, :all
    else
      can :read, :all
      can :create, Topic
      can :create, Post, :topic => {:deleted => false, :closed => false}
      can :update, Post, :user_id => user.id, :deleted => false, :topic => {:deleted => false, :closed => false}
      can :update, Topic, :user_id => user.id, :deleted => false, :closed => false
    end



    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user 
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. 
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
