class Topic < ActiveRecord::Base

  attr_accessible :forum_id, :last_post, :subject
  has_many :posts
  belongs_to :category
  belongs_to :user
  validates :subject, presence: true
  validates :user_id, presence: true
  validates_length_of :subject, :maximum => 25

  def to_param
    "#{id}-#{self.subject.parameterize}"
  end

end
