class Category < ActiveRecord::Base

  attr_accessible :name, :post_count, :topic_count_count
  validates :name, presence: true
  before_save :set_stats
  has_many :topics

  def to_param
    "#{id}-#{name.parameterize}"
  end

  protected

  def set_stats
    if self.new_record?
      self.post_count = 0
      self.topic_count = 0
    end
  end

end
