class Post < ActiveRecord::Base

  paginates_per 10

  attr_accessible :body, :topic_id, :user_id

  validates :user_id, presence: true
  validates :body, presence: true

  belongs_to :user, counter_cache: true
  belongs_to :topic, counter_cache: true

  before_save :parse_body

  def parse_body
    renderer = RooBBRender.new(
        :hard_wrap => true,
        :filter_html => true,
        :no_style => true,
        :safe_inks_only => true,
    )
    parser = Redcarpet::Markdown.new(renderer)
    self.parsed_body = parser.render(self.body)
  end

  class RooBBRender < Redcarpet::Render::HTML

    def postprocess (document)
      document.gsub(/\@\((.*?)\)/, '<cite>\1</cite>')
      document.gsub(/\@\((.*?)\|(.*?)\)/, '<cite><a href="#post-\2">\1</a></cite>')
    end

  end

end
