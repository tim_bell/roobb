prep_quote = (user, post, text) ->
    text = '\n> ' + text
    text = text + '\n@(' + user + '|' + post + ')'

$ ->
    $('.edit_button').click ->
        path = $(this).data('path')
        button = $(this)
        $.get path,
        (data) ->
            edit_container = button.closest('.post-body-block').children('.edit-post-wrap')
            body_container = button.closest('.post-body-block').children('.post-body-wrap')
            edit_container.html(data)
            body_container.hide()
            edit_container.show()
            button.addClass('disabled')

    $(document).on 'click', '.cancel-edit', (event) ->
        container = $(this).closest('.post-body-block')
        container.find('.edit-post-wrap').empty()
        container.find('.edit-post-wrap').hide()
        container.find('.post-body-wrap').show()
        container.find('.edit_button').removeClass('disabled')

    $(document).on 'submit', '.edit_post', (event) ->
        event.preventDefault();
        form = $(this)
        path = form.attr('action')
        data = form.serialize()
        $.post path, data, (data) ->
            container = form.closest('.post-body-block')
            container.find('.edit-post-wrap').empty()
            container.find('.edit-post-wrap').hide()
            container.find('.post-body-wrap').show()
            container.find('.post-body-wrap').html(data)
            container.find('.edit_button').removeClass('disabled')

    $('.reply_button').click ->
        text = ''
        user = $(this).data('user')
        id = $(this).data('id')
        if window.getSelection
            text = window.getSelection().toString()
        else if document.selection && document.selection.type != "Control"
            text = document.selection.createRange().text
        if text == ''
            alert('You must highlight some text')
            false
        else
            $('#post_body').append(prep_quote(user, id, text))

    $('.edit-subject').click ->
        $('.edit-subject').hide()
        $('.edit-subject-block').show()

    $(document).on 'click', '#save-subject', (event)  ->
        event.preventDefault();
        content = $('.edit-subject-block input').val()
        data = {subject: content}
        path = $('.subject-link-wrap').data('url')
        $.post path, data, (returned) ->
            $('.flash-wrap').append(returned)

    $('.subject-link-wrap, .popover').hover(
        (event) ->
            $(this).popover('show')
        (event) ->
            $(this).popover('hide')
    )

    $('.delete_trigger').click (event)->
        button = $(this)
        path = button.data('path')
        $.get path, (data) ->
            $('#delete-modal .modal-payload').html(data)
            $('#delete-modal').modal('show')

    $('.show-deleted-post').click (event)->
        block = $(this).closest('.post-deleted').children('.post-block').toggle()