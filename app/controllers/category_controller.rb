class CategoryController < ApplicationController

  load_and_authorize_resource :only => [:new, :create]

  def list
    @categories = Category.all
  end

  def show
    @category = Category.find(params[:id])
    @topics = Topic.scoped_by_category_id(@category.id).order('updated_at DESC').page(params[:page])
    add_breadcrumb @category.name, category_path(@category)
  end

  def new
    @category = Category.new()
    add_breadcrumb t('category.create')
  end

  def create
    @category = Category.new(params[:category])
    if @category.save
      redirect_to '/'
      flash[:success] = t('category.created')
    else
      render :action => 'new'
    end
  end
end
