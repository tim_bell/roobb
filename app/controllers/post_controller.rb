class PostController < ApplicationController


  def create
    authorize! :create, Post
    @category = Category.find(params[:category_id])
    @topic = Topic.find(params[:topic_id])
    @post = Post.new(params[:post])
    @post.topic_id = @topic.id
    @post.user_id = current_user.id

    if @post.save
      flash[:success] = "Post created"
    else
      flash[:error] = "There was a problem with your post"
    end

    redirect_to [@category, @topic]
  end

  def get_raw
    @post = Post.includes(topic: [:category]).find(params[:id])
    authorize! :update, @post
    render partial: 'get_raw'
  end

  def save_edit
    @post = Post.find(params[:id])
    authorize! :update, @post
    @post.body = params[:post][:body]
    @post.save
    render text: @post.parsed_body
  end

  def delete
    @post = Post.includes(:topic => [:category]).find(params[:id])
    authorize! :delete, @post
    @post.deleted = true;
    @post.deleted_message = params[:post][:deleted_message]
    if @post.save
      flash[:success] = "Post deleted"
    else
      flash[:error] = "There was a problem..."
    end
    redirect_to [@post.topic.category, @post.topic]
  end

  def restore
    @post = Post.includes(:topic => [:category]).find(params[:id])
    authorize! :delete, @post
    @post.deleted = false;
    @post.deleted_message = ''
    if @post.save
      flash[:success] = "Post restored"
    else
      flash[:error] = "There was a problem..."
    end
    redirect_to [@post.topic.category, @post.topic]
  end

  def confirm_delete
    @post = Post.includes(:topic => [:category]).find(params[:id])
    authorize! :delete, @post
    render :partial => 'confirm_delete'
  end

end
