class ApplicationController < ActionController::Base
  protect_from_forgery
  add_breadcrumb :index, :root_path

  def render_alert(mode, message)

    div_class = "alert"

    case mode
      when :error
        div_class += " alert-error"
      when :success
        div_class += " alert-success"
      when :info
        div_class += " alert-info"
    end

    body = '<div class="' + div_class + '">' + message +'<a class="close" data-dismiss="alert" href="#">&times;</a></div>'

    render(:text => body)
  end


end
