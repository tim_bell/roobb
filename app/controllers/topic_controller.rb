class TopicController < ApplicationController

  load_and_authorize_resource :only => [:new, :create]

  def show
    @topic = Topic.includes('category').find(params[:id])
    @category = @topic.category
    @posts = Post.scoped_by_topic_id(@topic.id).order(:created_at).page(params[:page]).includes(:user)
    @post = Post.new
    @post.topic = @topic
    add_breadcrumb @category.name, @category
    add_breadcrumb @topic.subject
  end

  def new
    @category = Category.find(params[:category_id])
    @topic = Topic.new
    @post = Post.new
    add_breadcrumb @category.name, @category
    add_breadcrumb t('topic.create')
  end

  def create
    @category = Category.find(params[:category_id])
    @topic = Topic.new(params[:topic])
    @topic.category_id = @category.id
    @topic.posts.build(params[:post])
    @topic.user_id = current_user.id
    @topic.posts[0].user_id = current_user.id
    if @topic.save
      redirect_to [@category, @topic]
      flash[:success] = t('topic.created')
    else
      @post = @topic.posts[0]
      render :action => 'new'
    end
  end

  def save_subject
    @topic = Topic.find(params[:id])
    authorize! :update, @topic
    @topic.subject = params[:subject]
    if @topic.save
      render_alert :success, t('topic.subject_updated')
    else
      render_alert :error, t('error')
    end

  end

  def delete
    @topic = Topic.includes(:category).find(params[:id])
    authorize! :delete, @topic
    @topic.deleted = true
    @topic.deleted_message = params[:topic][:deleted_message]
    if @topic.save
      flash[:success] = t('topic.deleted')
    else
      flash[:error] = t('error')
    end
    redirect_to [@topic.category, @topic]
  end

  def confirm_delete
    @topic = Topic.includes(:category).find(params[:id])
    authorize! :delete, @topic
    render :partial => 'confirm_delete'
  end

  def restore
    @topic = Topic.includes(:category).find(params[:id])
    authorize! :delete, @topic
    @topic.deleted = false
    @topic.deleted_message = ''
    if @topic.save
      flash[:success] = t('topic.restored')
    else
      flash[:error] = t('error')
    end
    redirect_to [@topic.category, @topic]
  end

  def close
    @topic = Topic.includes('category').find(params[:id])
    authorize! :delete, @topic

    @topic.toggle :closed

    if @topic.save
      if @topic.closed
        flash[:success] = t('topic.closed')
      else
        flash[:success] = t('topic.opened')
      end
    else
       flash[:error] = t('error')
    end
    redirect_to [@topic.category, @topic]
  end
end
