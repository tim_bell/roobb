class UsersController < ApplicationController

  def index
    @users = User.page(params[:page])
    add_breadcrumb t('users.list')
  end

  def show
    @user = User.find(params[:id])
    add_breadcrumb t('users.list', users_path)
    add_breadcrumb @user.username
  end

end
