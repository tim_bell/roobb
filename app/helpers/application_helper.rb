module ApplicationHelper


end

module ActionView
  module Helpers
    module FormTagHelper
      def submit_tag(value = "Save changes", options = {})
        options = options.stringify_keys

        if disable_with = options.delete("disable_with")
          options["data-disable-with"] = disable_with
        end

        if confirm = options.delete("confirm")
          options["data-confirm"] = confirm
        end

        if options.include?('class')
          options["class"] += ' btn'
        else
          options["class"] = 'btn'
        end
        tag :input, {"type" => "submit", "name" => "commit", "value" => value}.update(options)
      end
    end
  end
end


