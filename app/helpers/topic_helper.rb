module TopicHelper
  def get_topic_line (topic)
    text = topic.subject
    if topic.deleted && !topic.deleted_message.empty?
      text += ' - ' + topic.deleted_message
    end
    return text
  end

  def get_topic_class (topic)
    if topic.deleted
      return 'deleted'
    elsif topic.closed
      return 'closed'
    end
    return ''
  end
end
