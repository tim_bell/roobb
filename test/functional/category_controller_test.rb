require 'functional_test_case'

class CategoryControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  test "list" do
    get :list
    assert_response :success
    assert_not_nil assigns(:categories)
    categories = assigns(:categories)
    assert categories[0].name == 'Cats'
  end

  test "show" do
    get :show, :id => 5
    assert_response :success
    category = assigns(:category)
    assert_not_nil category
    assert category.name == 'dogs!cats%rats'
  end

  test "new" do
    @ability.can :create, Category
    get :new
    assert_response :success
    assert_not_nil assigns(:category)
  end

  test "create" do
    @ability.can :create, Category
    post :create, :category => {name: "rats and dogs"}
    assert_redirected_to root_path
  end

  test "no_perm" do
    assert_raises CanCan::AccessDenied do
      get :new
    end
    assert_raises CanCan::AccessDenied do
      post :create, :category => {name: "rats and dogs"}
    end
  end
end

