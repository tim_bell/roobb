require 'functional_test_case'

class PostControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  test "create" do
    @ability.can :create, Post
    user = User.find(15)
    sign_in user

    post :create, {'category_id' => 5, 'topic_id' => 3, 'post' => {'body' => 'hello fine world!'}}

    category = assigns(:category)
    topic = assigns(:topic)
    post = assigns(:post)

    assert post.user_id == 15
    assert post.body == 'hello fine world!'

    assert flash[:success] == "Post created"

    assert_redirected_to [category, topic]
  end

  test "create_failure" do
    @ability.can :create, Post
    user = User.find(15)
    sign_in user

    post :create, {'category_id' => 5, 'topic_id' => 3, 'post' => {'body' => ''}}

    category = assigns(:category)
    topic = assigns(:topic)
    post = assigns(:post)

    assert post.user_id == 15
    assert post.body == ''

    assert flash[:error] = "There was a problem with your post"

    assert_redirected_to [category, topic]
  end

  test "get_raw" do
    @ability.can :update, Post
    get :get_raw, {'category_id' => 5, 'topic_id' => 3, 'id' => 20}

    assert_response :success

    post = assigns(:post)
    assert_not_nil post
    assert post.body == "CatDog"
  end

  test "save_edit" do
    @ability.can :update, Post
    post :save_edit, {'category_id' => 5, 'topic_id' => 3, 'id' => 25, 'post' => {'body' => 'DogCat'}}
    assert_response :success
    assert response.body.include? '<p>DogCat</p>'
    post = Post.find(25)
    assert post.parsed_body.include? '<p>DogCat</p>'

  end

  test "save_no_perm" do
    assert_raises CanCan::AccessDenied do
      post :save_edit, {'category_id' => 5, 'topic_id' => 3, 'id' => 25, 'post' => {'body' => 'DogCat'}}
    end
  end

  test "raw_no_perm" do
    assert_raises CanCan::AccessDenied do
      get :get_raw, {'category_id' => 5, 'topic_id' => 3, 'id' => 20}
    end
  end

end
