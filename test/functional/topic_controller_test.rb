require 'functional_test_case'

class TopicControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  test "show" do
    get :show, {'category_id' => 5, 'id' => 3}
    assert_response :success
    assert_not_nil assigns(:post)
    assert_not_nil assigns(:posts)
    assert_not_nil assigns(:topic)
    assert_not_nil assigns(:category)
    category = assigns(:category)
    topic = assigns(:topic)
    assert category.name == "dogs!cats%rats"
    assert topic.subject == "Green Eggs and Ham"
  end

  test "new" do
    @ability.can :create, Topic
    get :new, {'category_id' => 5}
    assert_response :success
    assert_not_nil assigns(:category)
    assert_not_nil assigns(:topic)
    assert_not_nil assigns(:post)
    assert assigns(:category).name == "dogs!cats%rats"
  end

  test "create" do
    @ability.can :create, Topic
    @ability.can :create, Post

    user = User.find(15)
    sign_in user

    post :create, {'post' => {'body' => 'batdog'}, 'topic' => {'subject' => 'so important!'}, 'category_id' => 5}

    category = assigns(:category)
    topic = assigns(:topic)

    assert topic.subject = "so important!"
    assert topic.posts.size == 1
    assert topic.posts[0].user_id = 15

    assert_redirected_to [category, topic]
  end

  test "create no subject" do
    @ability.can :create, [Topic, Post]
    user = User.find(15)
    sign_in user

    post :create, {'post' => {'body' => 'batdog'}, 'topic' => {'subject' => ''}, 'category_id' => 5}

    assert_response :success

    category = assigns(:category)
    topic = assigns(:topic)

    assert topic.subject = "so important!"
    assert topic.posts.size == 1
    assert topic.posts[0].user_id = 15

    assert_not_empty topic.errors
  end

  test "create_no_perm" do
    assert_raises CanCan::AccessDenied do
      post :create, {'post' => {'body' => 'batdog'}, 'topic' => {'subject' => 'so important!'}, 'category_id' => 5}
    end
  end

  test "new_no_perm" do
    assert_raises CanCan::AccessDenied do
      get :new, {'category_id' => 5}
    end
  end

  test "update subject" do
    assert_raises CanCan::AccessDenied do
      post :save_subject, {'category_id' => 5, 'id' => 10, 'subject' => 'Ham and Green Eggs'}
    end

    @ability.can :update, [Topic]
    post :save_subject, {'category_id' => 5, 'id' => 10, 'subject' => 'Ham and Green Eggs'}

    topic = assigns(:topic)
    assert_not_nil topic

    assert_response :success

    topic = Topic.find(10)
    assert topic.subject == 'Ham and Green Eggs'
  end

end
