require 'test_helper'

class RegisterFlowsTest < ActionDispatch::IntegrationTest
  include Capybara::DSL

  test "register" do
    visit '/users/sign_up'
    assert page.has_content? 'Sign up'

    fill_in('Username', :with => 'John Smith')
    fill_in('Email', :with => 'jsmith@email.com')
    fill_in('Password', :with => 'testtest')
    fill_in('Password confirmation', :with => 'dsfuh')
    click_on('Sign up')
    assert page.has_content? "Password doesn't match confirmation"

    fill_in('Password', :with => 'testtest')
    fill_in('Password confirmation', :with => 'testtest')
    click_on('Sign up')

    assert find('.dropdown-menu').has_content?('Log out')
    assert !page.has_content?('Add category')

    find('.dropdown-menu').find_link('Account').click

    assert page.has_content?('Edit User')
    assert find_field('Username').value == 'John Smith'

    userRecord = User.find_by_email 'jsmith@email.com'
    userRecord.role = 1
    userRecord.save

    visit '/'

    assert page.has_content?('Add category')

    click_on 'Log out'

    assert find('.alert').has_content?('Signed out successfully.')

  end

  test "log in" do
    visit '/users/sign_in'
    assert page.has_content? 'Sign in'

    fill_in('Email', :with => 'test@test.com')
    fill_in('Password', :with => 'testtest')

    find('#new_user').find_button('Sign in').click

    assert page.has_content?('Signed in successfully.')

    click_on 'Log out'
  end

end