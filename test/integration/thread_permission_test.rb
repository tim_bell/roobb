require 'test_helper'

class ThreadPermissionTest < ActionDispatch::IntegrationTest
  include Capybara::DSL

  test "unregistered" do
    Capybara.reset_sessions!
    visit '/'
    assert page.has_content?('Home')
    assert !page.has_content?('Add category')

    visit '/category/2-cats'
    assert page.has_content?('Cats')
    assert !page.has_content?('New topic')

    visit '/category/2-cats/topic/5-to-be-or-not-to-be'
    assert page.has_content?('To be or not to be?')
    assert !page.has_content?('New Post')

    assert !page.has_selector?('.reply_button')
    assert !page.has_selector?('.report_button')
    assert !page.has_selector?('.edit_button')
    assert !page.has_selector?('.delete_button')
  end

  test "admin" do
    Capybara.reset_sessions!
    login('test@test.com', 'testtest')
    visit '/'
    assert page.has_content?('Home')
    assert page.has_content?('Add category')

    visit '/category/2-cats'
    assert page.has_content?('Cats')
    assert page.has_content?('New topic')

    visit '/category/2-cats/topic/5-to-be-or-not-to-be'
    assert page.has_content?('To be or not to be?')
    assert page.has_content?('New Post')

    assert page.has_selector?('.reply_button')
    assert page.has_selector?('.report_button')
    assert page.has_selector?('.edit_button')
    assert page.has_selector?('.delete_trigger')

    assert page.has_selector?('.edit_button', count: 2)

    assert find('#post-26').has_selector?('.edit_button')
    assert find('#post-27').has_selector?('.edit_button')

  end

  test "registered" do
    Capybara.reset_sessions!
    login('registered@test.com', 'testtest')
    visit '/'
    assert page.has_content?('Home')
    assert !page.has_content?('Add category')

    visit '/category/2-cats'
    assert page.has_content?('Cats')
    assert page.has_content?('New topic')

    visit '/category/2-cats/topic/5-to-be-or-not-to-be'
    assert page.has_content?('To be or not to be?')
    assert page.has_content?('New Post')

    assert page.has_selector?('.reply_button')
    assert page.has_selector?('.report_button')
    assert page.has_selector?('.edit_button')
    assert !page.has_selector?('.delete_button')

    assert page.has_selector?('.edit_button', count: 1)
    assert !find('#post-26').has_selector?('.edit_button')
    assert find('#post-27').has_selector?('.edit_button')
  end

  private

  def login (email, password)
    visit '/users/sign_in'
    assert page.has_content? 'Sign in'

    fill_in('Email', :with => email)
    fill_in('Password', :with => password)

    find('#new_user').find_button('Sign in').click
  end

end