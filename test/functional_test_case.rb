require 'test_helper'
require 'mocha/setup'

class ActiveSupport::TestCase
  def setup
    @ability = Object.new
    @ability.extend(CanCan::Ability)
    @controller.stubs(:current_ability).returns(@ability)
  end

end