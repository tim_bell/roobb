require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  test "zero stats" do
    model = Category.new()
    model.name = "cats and dogs"
    model.save

    assert model.topic_count == 0
    assert model.post_count == 0
  end

  test "compulsory name"  do
    model = Category.new
    assert !model.save()
  end

  test "seo url" do
    model = Category.find(5)
    assert model.to_param == "5-dogs-cats-rats"
  end

end
