require 'test_helper'

class PostTest < ActiveSupport::TestCase

  test "required fields" do
    post = Post.new
    assert !post.save
    post.topic_id = 3
    assert !post.save
    post.user_id = 15
    assert !post.save
    post.body = "Hello world"
    assert post.save
  end

  test "user relation" do
    post = Post.find(10)
    assert_not_nil post.user
    assert post.user.username == 'Joe Blow'
  end

  test "post values" do
    post = Post.find(10)
    assert post.body == "MyText"
    assert post.topic_id == 3
    assert post.user_id == 15
  end

  test "topic relation" do
    post = Post.find(10)
    assert_not_nil post.topic
    assert post.topic.subject == "Green Eggs and Ham"
  end

end
