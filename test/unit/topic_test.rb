require 'test_helper'

class TopicTest < ActiveSupport::TestCase

  test "category relation" do
    model = Topic.find(3)
    assert model.subject == 'Green Eggs and Ham'
    assert model.category.name == 'dogs!cats%rats'
  end

  test "post count" do
    model = Topic.find(3)
    assert model.posts.count == 4
  end

  test "seo url" do
    model = Topic.find(3)
    assert model.to_param == '3-green-eggs-and-ham'
  end

  test "subject required" do
    model = Topic.new()
    assert !model.save
    model.subject = "Cats and dogs"
    model.user_id = 1
    assert model.save
  end

end
